# coding: utf-8
require 'open-uri'

module BanescoChecker
  class Model
    extend BanescoChecker::BanescoRelation

    TRANSFERENCIA_OTRO_BANCO = "TRF DESDE OTRO BCO"
    CODIGO_BANESCO = "0134"
		INGRESO = '+'
		EGRESO = '-'
    # Variables de los registros del banco
    attr_accessor :fecha, :referencia, :banco, :monto, :tipo
    
    # Para inicializar el objeto, se puede crear con @fecha, @referencia, @banco y @monto nulo
    # o puede crearse pasandole la linea de donde va a extraer todos los datos. 
    # dicha linea debe tener los parametros separados por ; y en el siguiente orden
    # fecha; referencia; descripcion; monto; total
    def initialize(*params)
			@all_types = false
      case 
      when params.size == 5 
        @fecha, @referencia, @banco, @monto, @tipo = params[0], params[1], params[2], params[3], params[4]
			when params.size == 2
				read_line(params[0])
				@all_types = params[1]
			when params.size == 1
        read_line(params[0]) 
      end
    end

    def read_line(line)
      line_array = line.split(";").map! {|value| value.chomp.strip}
			unless line_array.size < 6
        @tipo = line_array[3][0]
        if @all_types || @tipo == INGRESO
          @fecha = line_array[0]
          @banco = extraer_banco(line_array)
          @referencia = extraer_referencia(line_array)
          @monto = line_array[3][1..-1]
        end
      end
    end

    def extraer_referencia(line_array)
      if line_array[2].include?(TRANSFERENCIA_OTRO_BANCO)
        if line_array[2][-4..-1] == '0108'
          line_array[2][-21..-16]
        else
          line_array[2][-11..-6]
        end 
      else
        line_array[1][-6..-1]
      end
    end

    def extraer_banco(line_array)
      if line_array[2].include?(TRANSFERENCIA_OTRO_BANCO)
        line_array[2][-4..-1]
      else
        CODIGO_BANESCO
      end
    end

    def empty?
      (@referencia.nil? || @fecha.nil? || @banco.nil? || @monto.nil?) ? true : false
    end

    def file 
      Model.file
    end

    def include?(params = {})
      t = []
      params.each do |key, value|
        t << (self.send(key) == value)
      end
      t.all?
    end

    def ==(another_model)
      [:referencia, :fecha, :monto, :banco].map do |v|
        self.send(v) == another_model.send(v)
      end.all?
    end
  end
end
