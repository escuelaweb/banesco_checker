
module BanescoChecker

  module BanescoRelation 
    @@url = nil
  
    def file
      open(@@url)
    end      

    def set_url(url)
      @@url = url
    end
      
    def all(all_types = false)
      todos = Relation.new
      file.each_line do |line|
        todos << Model.new(line, all_types)
      end
      todos.map!{ |p| p unless p.empty? }.compact!
    end

    def where(params = {})
      payments = (params[:all_types] ==  true) ? all(params[:all_types]) : all
			payments.where(params)
    end

    def where_in(by, array)
      payments = all
      payments.where_in(by, array)
    end
  end 

  class Relation < Array
    def where(params = {})
      where = self.clone
      where.map! do |payment|
        payment if payment.include?(params)
      end.compact!
    end

    def where_in(by, array)
      array.inject(Relation.new) do |todos, value|
        todos + self.where({ by => value })
      end
    end  

    def check_where(by, array)
      array.map do |value|
				{ value => !self.where({ by => value }).first.nil? }
      end
    end

		def set_array_params_to_hash(by, value)
			if by.kind_of?(Array) && value.kind_of?(Array)
				not_same_count_in_by_and_value_n(by, value)
				result = {}
			else 
				
			end
		end

		def not_same_count_in_by_and_value_n(one, two)
			result = two.map{ |t| t.count == one.count }.all?
			if result.nil?
				raise "The first params count is not the same amount"
			end
		end
  end
end
