require "spec_helper"


describe "BanescoChecker" do
  before do
    url = File.expand_path("../../data/j.txt", __FILE__)
    BanescoChecker::Model.set_url url
    @pagos = BanescoChecker::Model.all
  end
  
  it "When a array of references is send return array of false true" do
    references = ["157854", "312313", "165247"]
    checks = @pagos.check_where(:referencia, references)
    checks.should == [{ "157854" => true}, { "312313" => false}, { "165247" => true} ]
  end
end
