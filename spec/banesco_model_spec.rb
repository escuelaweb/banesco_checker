# coding: utf-8
require "spec_helper"

describe 'Individual Model (local)'  do
  before do
  end
  it 'On new have to return one class with :fecha, :referencia, :banco, :monto on nil' do
    pago = BanescoChecker::Model.new
    pago.fecha.should be_nil
    pago.referencia.should be_nil
    pago.banco.should be_nil
    pago.monto.should be_nil
  end
  
  it 'if the mount on line of payment if negative dont check it' do
    pago = BanescoChecker::Model.new
    line = "11/09/2013;29753715750;TRF TRANSFER/OTROS BANCOS     0102                               ;                 -1.200,00;                     27.678,66;"
    pago.read_line(line).should be_nil
  end

  it 'if the mount if positive and is from another bank(not banesco)' do
    line = "11/09/2013;29753715750;TRF DESDE OTRO BCO  94484974V004354197   0108                   ;                 +1.200,00;                     27.678,66;"
    pago = BanescoChecker::Model.new line
    pago.fecha.should == "11/09/2013"
    pago.referencia.should == "484974"
    pago.banco.should == "0108"
    pago.monto.should == "1.200,00"
  end

  it 'if the mount if positive and is from banesco' do
    line = "13/09/2013;00213269327;TRANS.CTAS                                                       ;                 +3.800,00;                     19.501,27;"
    pago = BanescoChecker::Model.new line
    pago.fecha.should == "13/09/2013"
    pago.referencia.should == "269327"
    pago.banco.should == "0134"
    pago.monto.should == "3.800,00"
  end

  it "Compare to model" do
    line = "13/09/2013;00213269327;TRANS.CTAS                                                       ;                 +3.800,00;                     19.501,27;"
    pago_1 = BanescoChecker::Model.new line
    pago_2 = BanescoChecker::Model.new line
    pago_1.referencia.should == pago_2.referencia
    pago_1.fecha.should == pago_2.fecha
    pago_1.banco.should == pago_2.banco
    pago_1.monto.should == pago_2.monto

    pago_1.should == pago_2
  end

  it "Find inside one payment with one value inside the pago" do
    line = "13/09/2013;00213269327;TRANS.CTAS                                                       ;                 +3.800,00;                     19.501,27;"
    pago = BanescoChecker::Model.new line
    pago.include?(fecha: "13/09/2013").should be_true
  end

  it "Find inside one payment with more than one value inside the pago" do
    line = "13/09/2013;00213269327;TRANS.CTAS                                                       ;                 +3.800,00;                     19.501,27;"
    pago = BanescoChecker::Model.new line
    pago.include?(fecha: "13/09/2013", referencia: "269327").should be_true
  end

  it "not Find inside one payment with one value inside the pago" do
    line = "13/09/2013;00213269327;TRANS.CTAS                                                       ;                 +3.800,00;                     19.501,27;"
    pago = BanescoChecker::Model.new line
    pago.include?(fecha: "13/09/2014").should be_false
  end

  it "not Find inside one payment with more than one value inside the pago" do
    line = "13/09/2013;00213269327;TRANS.CTAS                                                       ;                 +3.800,00;                     19.501,27;"
    pago = BanescoChecker::Model.new line
    pago.include?(fecha: "13/09/2014", referencia: "269327").should be_false
  end

  it "Can set and read the file of all payments" do
    file = File.expand_path("../../data/j.txt", __FILE__)
    BanescoChecker::Model.set_url file
    pago = BanescoChecker::Model.new  
    pago.file.path.should == file
  end

end

describe "Multiple Model (Local)" do
  before do
    @file = File.expand_path("../../data/j.txt", __FILE__)
    BanescoChecker::Model.set_url @file
  end

  it "Read all file and return a BanescoChecker::Relation" do
    pagos = BanescoChecker::Model.all
    pagos.should be_kind_of(BanescoChecker::Relation)
  end

  it "Seach where has to return a BanescoChecker::Relation" do
    pagos = BanescoChecker::Model.where(monto: "900,00")
    pagos.should be_kind_of(BanescoChecker::Relation)
  end

  it "can chain methods" do
    pagos = BanescoChecker::Model.all
    normal_pago = BanescoChecker::Model.where(monto: "900,00")
    pagos.where(monto: "900,00").should == normal_pago
  end

  it "Search all more than one, until dont restart url return same result" do
    pagos_1 = BanescoChecker::Model.all
    pagos_2 = BanescoChecker::Model.all
    pagos_1.should == pagos_2
  end

  it "Search payments by some field (:fecha)" do
    pagos = BanescoChecker::Model.where(fecha: "16/09/2013")
    pagos.count.should > 0
  end

  it "Search payments by some field (:monto)" do
    pagos = BanescoChecker::Model.where(monto: "900,00")
    pagos.count.should > 0
  end
  
  it "Seach and array of payments by fields" do
    monto_900 = BanescoChecker::Model.where(monto: "900,00")
    monto_3800 = BanescoChecker::Model.where(monto: "3.800,00")
    todos = monto_900 + monto_3800
    pagos = BanescoChecker::Model.where_in(:monto, ["900,00", "3.800,00"])
    pagos.should == todos
  end
end

describe "Multiple Model (remote file)" do
  before do
    @file = "https://dl.dropboxusercontent.com/u/113815053/banesco_chequer/j.txt"
    @file_local = File.expand_path("../../data/j.txt", __FILE__)
    BanescoChecker::Model.set_url @file
  end
  
  it "Initialize the file using a remote source" do
    BanescoChecker::Model.set_url @file
    file = BanescoChecker::Model.file
    file.should_not be_nil
  end
  
  # it "Search in local file should be equal to seach in remote file" do
  #   search_1 = BanescoChecker::Model.all
  #   BanescoChecker::Model.set_url @file_local
  #   search_2 = BanescoChecker::Model.all
  #   search_1.should == search_2
  # end

end
